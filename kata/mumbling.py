
# https://www.codewars.com/kata/mumbling

def accum_my(s):
    res = []
    for i in range(len(s)):    
        res.append(s[i].upper() + ((s[i].lower()) * (i)))
    return "-".join(res)


#best solution
def accum_best(s):
    return '-'.join(c.upper() + c.lower() * i for i, c in enumerate(s))

#print acaccum_mycum('RqaEzty');

arr = ['a', 'b', 'c']

for i,c in enumerate(arr): print i,c

# link: https://www.codewars.com/kata/count-the-number-of-cubes-with-paint-on

def count_squares(cuts):
    total = (cuts+1)**3
    inside = (cuts-1)**3 if cuts > 1 else 0
    return total - inside  


res = count_squares(5)

print res
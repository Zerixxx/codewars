

class Plugboard(object):
    def __init__(self, wires = ""):
        self.pairs = {}
        for i in range(0, len(wires), 2):
            if self.pairs.has_key(wires[i]) or self.pairs.has_key(wires[i+1]):
                raise Exception("key exists!")
            self.pairs[wires[i]] = wires[i+1]
            self.pairs[wires[i+1]] = wires[i]
        if len(self.pairs) > 20: 
            raise Exception("more then 20 pairs!")
                         
    def process(self, c):
        return self.pairs.get(c,c)


p = Plugboard("ABCDEFGHIJKLMNOPQRSTUV")

print p.process('A')
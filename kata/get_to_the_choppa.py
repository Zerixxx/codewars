
# https://www.codewars.com/kata/get-to-the-choppa
# python 3.4
# ready but not published


class Node(object):
    def __init__(self, x, y, passable = True):
        self.position = (x,y)
        self.passable = passable
        self.prev = None
        self.min_dist = 10000


def find_shortest_path(grid, start_node, end_node):
    paths, q = {}, [start_node]

    if not start_node.passable or not end_node.passable:
        raise ValueError ("Invalid start/end nodes")
    
    q[0].min_dist = 1

    while(len(q) > 0):
        if end_node == q[0]:
            return q    
        else:
            x,y = q[0].position
            for i in [(x+1,y), (x-1,y), (x,y+1), (x,y-1)]:
                allowed = next((n for n in grid if n.passable and n.position == i), None)
                if allowed is not None and q[0].min_dist + 1 < allowed.min_dist:
                    allowed.min_dist = q[0].min_dist + 1;
                    allowed.prev = q[0]
                    q.append(allowed)
            del q[0]

    return {}


grid = [
    Node(0,0),Node(0,1,False),Node(0,2),Node(0,3),
    Node(1,0),Node(1,1, False),Node(1,2),Node(1,3),
    Node(2,0),Node(2,1,False),Node(2,2),Node(2,3),
    Node(3,0),Node(3,1),Node(3,2),Node(3,3)
]

p = find_shortest_path(grid, grid[0], grid[10])

n = p[0]
while(n.prev is not None):
    print(n.position)
    n = n.prev



#link: https://www.codewars.com/kata/find-the-minimum-number-divisible-by-integers-of-an-array-i

def min_special_mult(arr):
    errors = []
    for i in range(len(arr)):
        try:
            arr[i] = int(arr[i])
        except ValueError:
            errors.append(num)
        except TypeError:
            arr[i] = 1

    if len(errors) == 1:
        return "There is {0} invalid entry: {1}".format(1, errors[0])
    elif len(errors) > 1:
        return "There are {0} invalid entries: {1}".format(len(errors), errors)
    
    for i in range(len(arr)-1):
        a = min(arr[i],arr[i+1])    #nsd
        b = max(arr[i],arr[i+1])
        while b % a != 0:
            a, b = b % a, a
        arr[i+1] = arr[i]*arr[i+1]/a
        
    return arr[len(arr)-1]


#420
arr = [16, 15, 23, '-012']
print min_special_mult(arr)

        


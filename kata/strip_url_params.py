

# https://www.codewars.com/kata/strip-url-params
# python 3.4

def strip_url_params(url, params_to_strip = []):
    if '?' not in url:
        return url

    parts = (url.split('?')[1]).split('&')
    base_url, args = parts[0] + '?', {}
    
    for arg in parts[1:]:
        k_v = arg.split('=')
        if not args.get(k_v[0]) and k_v[0] not in params_to_strip:
            args[k_v[0]] = k_v[1]

    base_url += ''.join('{}={}&'.format(key, val) for key, val in sorted(args.items()))
    return base_url[:-1]


s = strip_url_params('www.codewars.com?a=1&b=2&a=2', ['b'])
print(s)

